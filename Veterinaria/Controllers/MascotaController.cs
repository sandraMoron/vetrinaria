﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Veterinaria.Models;

namespace Veterinaria.Controllers
{
    public class MascotaController : Controller
    {
		private VeterinariaEntities db = new VeterinariaEntities();

		// GET: Mascotas
		public ActionResult Index()
		{
			if (Session["IdUsuario"] != null)
			{
				int idUsuario = Convert.ToInt32(Session["IdUsuario"]);
				string codigoPerfil = Session["CodigoPerfil"].ToString();
				var mascota = db.Mascota.Include(m => m.Usuario).Include(m => m.Sexo);
				if (codigoPerfil == "Cliente")
				{
					mascota = mascota.Where(m => m.IdUsuario == idUsuario);
				}
				
				return View(mascota.ToList().Take(5));
			}else return RedirectToAction("Login", "Usuario"); 
		}

	
		public ActionResult Detalles(int? id)
		{
			if (Session["IdUsuario"] != null)
			{
				if (id == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
				Mascota mascota = db.Mascota.Find(id);
				if (mascota == null)
				{
					return HttpNotFound();
				}

				return View(mascota);
			}else return RedirectToAction("Usuario", "Login");			

		}

		
		public ActionResult Crear(int? id)
		{
			ViewBag.IdSexo = new SelectList(db.Sexo, "IdSexo", "NombreSexo");

			if (id == null)
			{
				
				return View();
			}
			Mascota mascota = db.Mascota.Find(id);
			if (mascota == null)
			{				
				return View();
			}

			CargarViewBag(mascota);
			
			return View(mascota);
		}

	

		[HttpPost]
		public ActionResult Crear(Mascota mascota)
		{
			if (Session["IdUsuario"] != null)
			{
				CargarViewBag(mascota);
				mascota.IdUsuario = Convert.ToInt32(Session["IdUsuario"]);
				
				if (mascota.IdMascota == 0)
				{
					db.Mascota.Add(mascota);
				}
				else
				{
					db.Entry(mascota).State = EntityState.Modified;					
				}

					db.SaveChanges();

				return RedirectToAction("Index");
			}
			else { return RedirectToAction("Usuario", "Login"); }
			
		}

	
		public ActionResult Eliminar(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Mascota mascota = db.Mascota.Find(id);
			if (mascota == null)
			{
				return HttpNotFound();
			}
			return View(mascota);
		}

		// POST: Mascotas/Delete/5
		[HttpPost, ActionName("Eliminar")]

		public ActionResult EliminareConfirmado(int id)
		{
			Mascota mascota = db.Mascota.Find(id);
			db.Mascota.Remove(mascota);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		public void CargarViewBag(Mascota mascota)
		{
			if (mascota.IdMascota > 0)
			{

				ViewBag.IdSexo = new SelectList(db.Sexo, "IdSexo", "NombreSexo", mascota.IdSexo);
			}
		
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}