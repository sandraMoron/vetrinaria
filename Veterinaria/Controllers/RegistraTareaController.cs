﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Veterinaria.Models;

namespace Veterinaria.Controllers
{
	public class RegistraTareaController : Controller
	{
		private VeterinariaEntities db = new VeterinariaEntities();
		// GET: RegistraTarea
		public ActionResult Index()
		{
			int IdUsuario = Convert.ToInt32(Session["IdUsuario"]);
			var registrarTarea = db.RegistrarTarea.Include(r => r.Mascota).Include(r => r.Tarea);
			var mascotas = db.Mascota.Where(m => m.IdUsuario== IdUsuario);
			registrarTarea = registrarTarea.Where(r => mascotas.Contains(r.Mascota));
			return View(registrarTarea.ToList());
		}

		public ActionResult Detalles(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			RegistrarTarea registrarTarea = db.RegistrarTarea.Find(id);
			if (registrarTarea == null)
			{
				return HttpNotFound();
			}
			return View(registrarTarea);
		}

		public ActionResult Guardar(int? id)
		{
			

			if (Session["IdUsuario"] != null)
			{
				int IdUsuario = Convert.ToInt32(Session["IdUsuario"]);
			
				if (id == null)
				{
					ViewBag.IdMascota = new SelectList(db.Mascota.Where(m => m.IdUsuario == IdUsuario), "IdMascota", "Nombre");
					ViewBag.IdTipoTarea = new SelectList(db.TipoTarea, "IdTipoTarea", "Nombre");
					ViewBag.IdTarea = new SelectList(db.Tarea.Where(t => t.IdTipoTarea == db.TipoTarea.FirstOrDefault().IdTipoTarea), "IdTarea", "Nombre");
					return View();
				}
				else
				{
					RegistrarTarea rt = new RegistrarTarea();
					rt = db.RegistrarTarea.Where(t => t.IdRegistrar == id).FirstOrDefault();
					if (rt != null)
					{					
						ViewBag.IdMascota = new SelectList(db.Mascota.Where(m => m.IdUsuario == IdUsuario), "IdMascota", "Nombre", rt.IdMascota);			
						ViewBag.IdTarea = new SelectList(db.Tarea, "IdTarea", "Nombre", rt.IdTarea);
						ViewBag.IdTipoTarea = new SelectList(db.TipoTarea, "IdTipoTarea", "Nombre", db.TipoTarea.Where(t => t.IdTipoTarea == db.Tarea.Where(tt => tt.IdTarea == rt.IdTarea).FirstOrDefault().IdTipoTarea).FirstOrDefault().IdTipoTarea);
						return View(rt);
					}
					else return View();

				}
			}
			else return RedirectToAction("Login", "Usuario");
		}

		[HttpPost]
		public ActionResult Guardar(RegistrarTarea tarea)
		{
			if (Session["IdUsuario"] != null)
			{
				RegistrarTarea nueva = db.RegistrarTarea.Where(t => t.FechaTarea == tarea.FechaTarea && t.HoraTarea == tarea.HoraTarea).FirstOrDefault();
				if (nueva == null)
				{
					tarea.FechaTarea = Convert.ToDateTime(tarea.FechaTarea);
					if (tarea.IdRegistrar > 0)
					{
						//tarea.HoraTarea = new TimeSpan(tarea.HoraTarea);
						db.Entry(tarea).State = EntityState.Modified;
					}
					else
					{
						db.RegistrarTarea.Add(tarea);
					}

					db.SaveChanges();
					return RedirectToAction("Index");
				}
				else
				{
					ModelState.AddModelError("", "Ya una tarea para este día y hora");
					return View(tarea);
				}				
			}
			else return RedirectToAction("Login", "Usuario");
		}

		public ActionResult Eliminar(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			RegistrarTarea registrarTarea = db.RegistrarTarea.Find(id);
			if (registrarTarea == null)
			{
				return HttpNotFound();
			}
			return View(registrarTarea);
		}

		
		[HttpPost, ActionName("Eliminar")]

		public ActionResult EliminareConfirmado(int id)
		{
			RegistrarTarea registrarTarea = db.RegistrarTarea.Find(id);
			db.RegistrarTarea.Remove(registrarTarea);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

	}
}