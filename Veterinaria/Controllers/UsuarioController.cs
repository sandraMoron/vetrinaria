﻿//using Veterinaria.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Veterinaria.Models;

namespace Veterinaria.Controllers
{
    public class UsuarioController : Controller
    {
		private VeterinariaEntities db = new VeterinariaEntities();
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
		public ActionResult Login()
		{
			return View();
		}

		[HttpPost]
		public ActionResult Login(Usuario usuarioLogin)
		{
			try
			{
				if (ModelState.IsValid)
				{
					if (usuarioLogin.NombreUsuario != 0)
					{
						Usuario usuario = db.Usuario.Where(u => u.NombreUsuario == usuarioLogin.NombreUsuario && u.Clave == usuarioLogin.Clave).FirstOrDefault();
						if (usuario != null)
						{
							Session["IdUsuario"] = usuario.IdUsuario.ToString();
							Session["CodigoPerfil"] = db.Perfil.Where(p => p.IdPerfil == usuario.IdPerfil).FirstOrDefault().Codigo;
							ViewBag.NombreUsuario = usuario.NombreUsuario.ToString();
							Session["NombreUsuario"] = usuario.NombreUsuario.ToString();
							if (Session["CodigoPerfil"].ToString() == "Cliente")
							{
								return RedirectToAction("Crear", "Cliente", new { cedula= usuario.NombreUsuario });
							}
							else
							{
								return RedirectToAction("ListarClientes", "Cliente");
							}

							
						}
						else
						{
							ModelState.AddModelError("", "Credenciales inválidas");
							return View(usuarioLogin);
						}
					}
					return View();
				}

				
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("", ex.Message);
			
			}			

			return View(usuarioLogin);
		}


		public ActionResult Registrar()
		{		
			return View();
		}

		[HttpPost]
		public ActionResult Registrar(Usuario usuario)
		{
			usuario.IdPerfil = db.Perfil.Where(p => p.Codigo == "Cliente").FirstOrDefault().IdPerfil;
			if (db.Usuario.Select(u => u.NombreUsuario == usuario.NombreUsuario).Count() > 0)
			{
				db.Usuario.Add(usuario);
				db.SaveChanges();
				return RedirectToAction("Crear", "Cliente", new { cedula = usuario.NombreUsuario });
			}
			else
			{
				ModelState.AddModelError("", "Ya existe usuario");
				return View(usuario);
			}
			
		}

		[HttpPost]
		public ActionResult CerrarSesion()
		{
			Session["IdUsuario"] = null;
			ViewBag.NombreUsuaro = string.Empty;
			return RedirectToAction("Login");
		}
	}
}