﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Veterinaria.Models;

namespace Veterinaria.Controllers
{
	public class TiendaController : Controller
	{
		private VeterinariaEntities db = new VeterinariaEntities();
		// GET: Tienda
		public ActionResult Index()
		{
			return View(db.Tienda.ToList());
		}

		public ActionResult Crear()
		{
			ViewBag.IdCiudad = new SelectList(db.Ciudad, "IdCiudad", "Nombre");
			return View();
		}

		[HttpPost]
		public ActionResult Crear(Tienda tienda)
		{
			ViewBag.IdCiudad = new SelectList(db.Ciudad, "IdCiudad", "Nombre");
			string codigoPerfil = Session["CodigoPerfil"].ToString();
			if (codigoPerfil == "Admon")
			{
				if (ModelState.IsValid)
				{
					db.Tienda.Add(tienda);
					db.SaveChanges();
				}
			}

			return RedirectToAction("Index");
		}


		public ActionResult Editar(int id)
		{
			
			string codigoPerfil = Session["CodigoPerfil"].ToString();
			if (codigoPerfil == "Admon")
			{
				if (ModelState.IsValid)
				{
					Tienda editar = db.Tienda.Where(t => t.IdTienda == id).FirstOrDefault();
				
					if (editar != null)
					{
						ViewBag.IdCiudad = new SelectList(db.Ciudad, "IdCiudad", "Nombre", editar.IdCiudad);
						return View(editar);
					}
					else
					{
						ModelState.AddModelError("", "No existe");
						return View();
					}

				}
			}
			else
			{
				ModelState.AddModelError("", "No tiene permiso");
				
			}
			return View();
		}

		[HttpPost]
		public ActionResult Editar(Tienda tienda)
		{
			
			string codigoPerfil = Session["CodigoPerfil"].ToString();
			if (codigoPerfil == "Admon")
			{
				if (ModelState.IsValid)
				{
					db.Entry(tienda).State = EntityState.Modified;
					db.SaveChanges();
				}
			}

			return RedirectToAction("Index");
		}

		public ActionResult Detalles(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Tienda tienda = db.Tienda.Find(id);
			if (tienda == null)
			{
				return HttpNotFound();
			}
			return View(tienda);
		}

		public ActionResult Eliminar(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Tienda tienda = db.Tienda.Find(id);
			if (tienda == null)
			{
				return HttpNotFound();
			}
			return View(tienda);
		}

		// POST: Mascotas/Delete/5
		[HttpPost, ActionName("Eliminar")]

		public ActionResult EliminareConfirmado(int id)
		{
			Tienda tienda = db.Tienda.Find(id);
			db.Tienda.Remove(tienda);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		public ActionResult InfoTienda()
		{
			var info = (from registrarTarea in db.RegistrarTarea
					join mascota in db.Mascota on registrarTarea.IdMascota equals mascota.IdMascota				
					group new { mascota, registrarTarea} by new { registrarTarea.IdMascota } into grupo
					select new 
					{						
						IdMascota = grupo.Key.IdMascota,
						cantiad = grupo.Count()
					}).ToList();

			List<ClienteTarea> lista = new List<ClienteTarea>();
			foreach (var i in info)
			{
				Mascota mascota = db.Mascota.Where(m => m.IdMascota == i.IdMascota).FirstOrDefault();
				if (mascota != null)
				{
					Usuario usuario = db.Usuario.Where(u => u.IdUsuario == mascota.IdUsuario).FirstOrDefault();
					if (usuario != null)
					{
						Cliente cliente = db.Cliente.Where(c => c.Cedula == usuario.NombreUsuario).FirstOrDefault();
						if (cliente != null)
						{
							Tienda tienda = db.Tienda.Where(t => t.IdTienda == cliente.IdTienda).FirstOrDefault();
							ClienteTarea datos = new ClienteTarea();
							datos.Apodo = mascota.Apodo;
							datos.Cedula = cliente.Cedula;
							datos.Nombrecliente = cliente.Nombre;
							datos.NumTareas = (from tareas in db.RegistrarTarea
											   where tareas.IdMascota == i.IdMascota
											   select tareas).Count();
							datos.Tienda = tienda.Nombre;
							lista.Add(datos);
						}
						
					}
					
				}
				
							  
			}
			return View(lista);

		}
	}
}