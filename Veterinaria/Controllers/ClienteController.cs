﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Veterinaria.DAL;
using Veterinaria.Models;

namespace Veterinaria.Controllers
{
	public class ClienteController : Controller
	{
		
		private VeterinariaEntities db = new VeterinariaEntities();
		public ActionResult Index()
		{
			if (Session["IdUsuario"] != null)
			{
				int IdUsuario = Convert.ToInt32( Session["IdUsuario"]);
				List<MascotasRegistradas> lista = new List<MascotasRegistradas>();
				var l = from registrarTarea in db.RegistrarTarea
						join mascota in db.Mascota on registrarTarea.IdMascota equals mascota.IdMascota
						join tarea in db.Tarea on registrarTarea.IdTarea equals tarea.IdTarea
						where mascota.IdUsuario == IdUsuario
						group new { mascota, registrarTarea, tarea } by new { mascota.IdMascota } into grupo
						select new
						{
							CostoTotal = grupo.Sum(s => s.tarea.Valor),
							MascotaId = grupo.Key.IdMascota,
							FechaProximaTarea = grupo.Max(t => t.registrarTarea.FechaTarea)
						};

				foreach( var i in l)
				{
					MascotasRegistradas mascotasR = new MascotasRegistradas();
					Mascota mascota = db.Mascota.Where(m => m.IdMascota == i.MascotaId).FirstOrDefault();
					mascotasR.Apodo = mascota.Apodo;
					mascotasR.Nombre = mascota.Nombre;
					mascotasR.FechaProximaTarea = i.FechaProximaTarea;
					mascotasR.CostoTotal = i.CostoTotal;
					mascotasR.Raza = mascota.Raza;
					var tareaPendiete = from registrar in db.RegistrarTarea
										join mas in db.Mascota on registrar.IdMascota equals mas.IdMascota
										where DbFunctions.TruncateTime(registrar.FechaTarea) == DbFunctions.TruncateTime(DateTime.Now) && mas.IdUsuario == IdUsuario
										group mas by new { mas.IdMascota } into g
										select new { valor = g.Count() };

					var tareasCompletadas = from registrar in db.RegistrarTarea
										join mas in db.Mascota on registrar.IdMascota equals mas.IdMascota
										where DbFunctions.TruncateTime(registrar.FechaTarea) < DbFunctions.TruncateTime(DateTime.Now) && mas.IdUsuario == IdUsuario
											group mas by new { mas.IdMascota } into g
										select new { valor = g.Count() };

					var totalTareas = (from r in db.RegistrarTarea
									 where r.IdMascota == i.MascotaId select r).Count();

					try
					{
						mascotasR.NumTareasPendientesDia = tareaPendiete.FirstOrDefault().valor;
					}
					catch (Exception)
					{
						
					}
					try
					{
						mascotasR.PorcentajeTareasRealizadas = (100 * tareasCompletadas.FirstOrDefault().valor) / totalTareas;
					}
					catch (Exception)
					{
						
					}
					
					
					lista.Add(mascotasR);
				}
				return View(lista);

			}
			else return RedirectToAction("Login", "Usuario");

		
		}

		public ActionResult ListarClientes()
		{
			string codigoPerfil = Session["CodigoPerfil"].ToString();
			if (codigoPerfil == "Admon")
			{
				return View(db.Cliente.ToList());
			}
			return View();
		}

		public ActionResult Crear(int? cedula)
		{
			//if (Session["IdUsuario"] != null)
			//{
				int cedulaSesion = Convert.ToInt32(Session["NombreUsuario"]);
				if (cedulaSesion > 0)
				{
					Cliente cli = new Cliente();
					cli = db.Cliente.Where(c => c.Cedula == cedulaSesion).FirstOrDefault();
					if (cli != null)
					{
						ConfigurarViewBag(cli);
						return View(cli);
					}

				}

				ConfigurarViewBag(new Cliente());
			Cliente cliente = new Cliente();
			if (cedula > 0)
			{
				cliente.Cedula = cedula.Value;
			}
				return View(cliente);
			//}
			//else return RedirectToAction("Login", "Usuario");
		}

		[HttpPost]
	
		public ActionResult Crear(Cliente cliente)
		{
			//if (ModelState.IsValid)
			//{
			string codigoPerfil = Session["CodigoPerfil"].ToString();
				if (cliente.IdCliente == 0)
				{
					db.Cliente.Add(cliente);
				}
				else
				{
					db.Entry(cliente).State = EntityState.Modified;
				}

				db.SaveChanges();
			ConfigurarViewBag(cliente);
			//return RedirectToAction("Index");
			if (codigoPerfil == "Admon")
			{
				return RedirectToAction("ListarClientes");
			}
			else return View(cliente);
			//}

		
			//return View(cliente);
		}

		public ActionResult Editar(int id)
		{
			Cliente cli = new Cliente();
			cli = db.Cliente.Where(c => c.IdCliente == id).FirstOrDefault();
			if (cli != null)
			{
				ConfigurarViewBag(cli);
				return View(cli);
			}
			return View();
		}

		public void ConfigurarViewBag(Cliente cli)
		{
			if (cli.IdCliente > 0)
			{
				ViewBag.IdTienda = new SelectList(db.Tienda, "IdTienda", "Nombre", cli.IdTienda);
				ViewBag.IdSexo = new SelectList(db.Sexo, "IdSexo", "NombreSexo", cli.IdSexo);
				ViewBag.IdCiudad = new SelectList(db.Ciudad, "IdCiudad", "Nombre", db.Tienda.Where(t => t.IdTienda == cli.IdTienda).FirstOrDefault().IdCiudad);
			}
			else
			{
				ViewBag.IdSexo = new SelectList(db.Sexo, "IdSexo", "NombreSexo");
				ViewBag.IdCiudad = new SelectList(db.Ciudad, "IdCiudad", "Nombre");

				ViewBag.IdTienda = new SelectList(db.Tienda.Where(t => t.IdCiudad == db.Ciudad.FirstOrDefault().IdCiudad), "IdTienda", "Nombre");
			}

		}

		public JsonResult GetStoreByCity(int id)
		{
			List<Tienda> lista = new List<Tienda>();
			lista = db.Tienda.Where(t => t.IdCiudad == id).ToList();
			SelectList listaSelect = new SelectList(lista, "IdTienda", "Nombre");
			return Json(listaSelect);

		}
	}
}