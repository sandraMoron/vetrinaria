USE [Veterinaria]
GO

/****** Object:  Table [dbo].[TipoTarea]    Script Date: 23/05/2017 6:00:31 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TipoTarea](
	[IdTipoTarea] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](150) NULL,
 CONSTRAINT [PK_TipoTarea] PRIMARY KEY CLUSTERED 
(
	[IdTipoTarea] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


