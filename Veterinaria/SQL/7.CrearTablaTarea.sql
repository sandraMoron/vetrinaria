USE [Veterinaria]
GO

/****** Object:  Table [dbo].[Tarea]    Script Date: 23/05/2017 6:00:05 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Tarea](
	[IdTarea] [int] IDENTITY(1,1) NOT NULL,
	[Valor] [float] NOT NULL,
	[Nombre] [nvarchar](150) NOT NULL,
	[IdTipoTarea] [int] NOT NULL,
 CONSTRAINT [PK_Tarea] PRIMARY KEY CLUSTERED 
(
	[IdTarea] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Tarea]  WITH CHECK ADD  CONSTRAINT [FK_Tarea_TipoTarea] FOREIGN KEY([IdTipoTarea])
REFERENCES [dbo].[TipoTarea] ([IdTipoTarea])
GO

ALTER TABLE [dbo].[Tarea] CHECK CONSTRAINT [FK_Tarea_TipoTarea]
GO


