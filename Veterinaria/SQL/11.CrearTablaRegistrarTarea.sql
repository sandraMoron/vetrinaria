USE [Veterinaria]
GO

/****** Object:  Table [dbo].[RegistrarTarea]    Script Date: 23/05/2017 5:59:36 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RegistrarTarea](
	[IdRegistrar] [int] IDENTITY(1,1) NOT NULL,
	[IdTarea] [int] NOT NULL,
	[IdMascota] [int] NOT NULL,
	[FechaTarea] [datetime] NOT NULL,
	[HoraTarea] [time](7) NOT NULL,
 CONSTRAINT [PK_RegistrarTarea] PRIMARY KEY CLUSTERED 
(
	[IdRegistrar] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RegistrarTarea]  WITH CHECK ADD  CONSTRAINT [FK_RegistrarTarea_Mascota] FOREIGN KEY([IdMascota])
REFERENCES [dbo].[Mascota] ([IdMascota])
GO

ALTER TABLE [dbo].[RegistrarTarea] CHECK CONSTRAINT [FK_RegistrarTarea_Mascota]
GO

ALTER TABLE [dbo].[RegistrarTarea]  WITH CHECK ADD  CONSTRAINT [FK_RegistrarTarea_Tarea] FOREIGN KEY([IdTarea])
REFERENCES [dbo].[Tarea] ([IdTarea])
GO

ALTER TABLE [dbo].[RegistrarTarea] CHECK CONSTRAINT [FK_RegistrarTarea_Tarea]
GO


