﻿using Veterinaria.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Infrastructure;

namespace Veterinaria.DAL
{
    public class VeterinariaContext : DbContext
    {
		//public VeterinariaContext()
  //          : base("name=VeterinariaContext")
  //      {
		//}
		//protected override void OnModelCreating(DbModelBuilder modelBuilder)
		//{
		//	//modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
		//	throw new UnintentionalCodeFirstException();
		//}


		public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Tienda> Tienda { get; set; }
        public DbSet<Tarea> Tarea { get; set; }
        public DbSet<TipoTarea> TipoTarea { get; set; }
        public DbSet<RegistrarTarea> RegistrarTarea { get; set; }
        public DbSet<Mascota> Mascota { get; set; }
        public DbSet<Sexo> Sexo { get; set; }

		public DbSet<Perfil> Perfil { get; set; }
		public DbSet<Menu> Menu { get; set; }
		public DbSet<Cliente> Cliente { get; set; }
		public DbSet<Ciudad> Ciudad { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

		}


	}
}