﻿using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace Veterinaria.DAL
{
    public class VeterinariaConfiguration : DbConfiguration
    {
        public VeterinariaConfiguration()
        {
            SetExecutionStrategy("System.Data.SqlClient", () => new SqlAzureExecutionStrategy());
        }
    }
}