﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Veterinaria.Models
{
	public class MascotasRegistradas
	{
		public string Apodo { get; set; }
		public string Nombre { get; set; }
		public string Raza { get; set; }
		public double CostoTotal { get; set; }
		public DateTime FechaProximaTarea { get; set; }
		public int NumTareasPendientesDia { get; set; }
		public int PorcentajeTareasRealizadas { get; set; }
	}
}