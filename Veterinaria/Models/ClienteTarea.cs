﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Veterinaria.Models
{
	public class ClienteTarea
	{
		public int IdMascota { get; set; }
		public int Cedula { get; set; }
		public string Nombrecliente { get; set; }
		public string Tienda { get; set; }
		public string Apodo { get; set; }
		public int NumTareas { get; set; }
	}
}